package com.anoyi.config.server;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class ServerConfiguration {

    @Value("${server.servlet.application-display-name}")
    private String serverId;

    private String globalTopic = "IM_TOPIC";

    public String getGlobalTopic() {
        return this.globalTopic;
    }
}
