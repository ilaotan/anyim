package com.anoyi.config.websocket;

import com.anoyi.config.server.ServerConfiguration;
import com.anoyi.mongo.model.WebSocketSessionBean;
import com.anoyi.mongo.repository.WebSocketRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.socket.messaging.SessionConnectedEvent;

import java.security.Principal;

/**
 * 监听 WebSocket 连接建立的事件
 */
@Component
@AllArgsConstructor
@Data
@Log4j2
public class WebSocketConnectHandler implements ApplicationListener<SessionConnectedEvent> {

    private final WebSocketRepository webSocketRepository;

    private final ServerConfiguration serverConfiguration;

    private final SimpMessagingTemplate template;

    @Override
    public void onApplicationEvent(SessionConnectedEvent event) {
        MessageHeaders headers = event.getMessage().getHeaders();
        Principal principal = SimpMessageHeaderAccessor.getUser(headers);
        Assert.notNull(principal, "principal is null");
        String username = principal.getName();
        log.info("[建立连接] {} ", username);
        WebSocketSessionBean webSocketSessionBean = new WebSocketSessionBean(username, serverConfiguration.getServerId());
        webSocketRepository.save(webSocketSessionBean);
        // TODO 处理连接后的逻辑
    }
}
