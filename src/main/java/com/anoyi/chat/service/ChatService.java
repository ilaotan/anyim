package com.anoyi.chat.service;

import com.anoyi.chat.Model.BaseMessage;
import com.anoyi.chat.Model.ChatMessage;
import com.anoyi.config.server.ServerConfiguration;
import com.anoyi.mongo.model.UserBean;
import com.anoyi.mongo.model.WebSocketSessionBean;
import com.anoyi.mongo.repository.WebSocketRepository;
import com.anoyi.mongo.service.UserService;
import com.anoyi.tools.RobotTools;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import nats.client.Nats;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service
@AllArgsConstructor
@Log4j2
public class ChatService {

    private static final String ROBOT = "IM_ROBOT";

    private static final String ALL = "IM_ALL";

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM-dd HH:mm");

    private final SimpMessagingTemplate template;

    private final UserService userService;

    private final WebSocketRepository webSocketRepository;

    private final Nats nats;

    private final ServerConfiguration serverConfiguration;

    /**
     * 发送消息
     */
    public void sendMessage(BaseMessage baseMessage) {
        String receiver = baseMessage.getReceiver();
        switch (receiver) {
            case ROBOT:
                robotReply(baseMessage);
                break;
            case ALL:
                nats.publish(serverConfiguration.getGlobalTopic(), JSON.toJSONString(baseMessage));
                break;
            default:
                WebSocketSessionBean socketSessionBean = webSocketRepository.findByKey(baseMessage.getReceiver());
                String targetServerId = socketSessionBean.getValue();
                String serverId = serverConfiguration.getServerId();
                log.info("[消息][接受者: {}, 所在服务器: {}][当前服务器: {}]", socketSessionBean.getKey(), socketSessionBean.getValue(), serverId);
                if (serverId.equals(targetServerId)) {
                    // 联系人和自己在同一容器
                    sendToUser(JSON.toJSONString(baseMessage));
                } else {
                    // 联系人和自己在不同容器
                    nats.publish(targetServerId, JSON.toJSONString(baseMessage));
                }
                break;
        }
    }

    /**
     * 发送给用户
     */
    public void sendToUser(String baseMessage) {
        JSONObject json = JSONObject.parseObject(baseMessage);
        ChatMessage chatMessage = createChatMessage(json.getString("sender"), json.getString("content"));
        log.info("[发送][个人消息][{} -> {}] <{}>", chatMessage.getUsername(), json.getString("receiver"), chatMessage.getContent());
        template.convertAndSendToUser(json.getString("receiver"), "/topic/chat", JSON.toJSONString(chatMessage));
    }

    /**
     * 发送给所有人
     */
    public void sendToAll(String baseMessage) {
        JSONObject json = JSONObject.parseObject(baseMessage);
        ChatMessage chatMessage = createChatMessage(json.getString("sender"), json.getString("content"));
        log.info("[发送][全服消息][{}] <{}>", json.getString("receiver"), chatMessage.getContent());
        template.convertAndSend("/topic/notice", JSON.toJSONString(chatMessage));
    }

    /**
     * 机器人回复
     */
    private void robotReply(BaseMessage baseMessage) {
        String replyContent = RobotTools.reply(baseMessage.getContent());
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setAvatar("http://img.hb.aicdn.com/79cf2c86f7c626ae5d135786b912803275fcd0c82423-LJyhZR_fw658");
        chatMessage.setNickname("AnyIM 小沫");
        chatMessage.setContent(replyContent);
        chatMessage.setSendTime(simpleDateFormat.format(new Date()));
        chatMessage.setUsername(ROBOT);
        log.info("[发送][机器人]" + baseMessage.getSender());
        template.convertAndSendToUser(baseMessage.getSender(), "/topic/chat", JSON.toJSONString(chatMessage));
    }

    private ChatMessage createChatMessage(String username, String message) {
        ChatMessage chatMessage = new ChatMessage();
        UserBean userBean = userService.getByUsername(username);
        chatMessage.setAvatar(userBean.getAvatar());
        chatMessage.setNickname(userBean.getNickname());
        chatMessage.setContent(message);
        chatMessage.setSendTime(simpleDateFormat.format(new Date()));
        chatMessage.setUsername(userBean.getUsername());
        return chatMessage;
    }

}
