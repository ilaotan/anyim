package com.anoyi.chat;

import com.anoyi.chat.service.ChatService;
import com.anoyi.config.server.ServerConfiguration;
import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import nats.client.Nats;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Log4j2
@AllArgsConstructor
@Component
public class GlobalMessageHandler implements InitializingBean{

    private final ChatService chatService;

    private final Nats nats;

    private final ServerConfiguration serverConfiguration;

    @Override
    public void afterPropertiesSet() throws Exception {
        nats.subscribe(serverConfiguration.getServerId(), message -> userMessage(message.getBody()));
        nats.subscribe(serverConfiguration.getGlobalTopic(), message -> globalMessage(message.getBody()));
    }

    // 接受其他服务的消息并转发
    private void userMessage(String message) {
        log.info("[收到][个人消息] <" + message + ">");
        chatService.sendToUser(message);
    }

    private void globalMessage(String message){
        log.info("[收到][全服消息] <" + message + ">");
        chatService.sendToAll(message);
    }

}
