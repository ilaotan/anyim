package com.anoyi.mongo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AllArgsConstructor
@Document
public class WebSocketSessionBean {

    // 用户 username
    private String key;

    // 服务器 ID
    private String value;

}
