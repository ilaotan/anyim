package com.anoyi.mongo.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@Document
public class RelationBean {

    @Id
    private String id;

    private String me;

    private String friend;

    public RelationBean(String me, String friend){
        this.me = me;
        this.friend = friend;
    }

}
