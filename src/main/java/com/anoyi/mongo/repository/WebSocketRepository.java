package com.anoyi.mongo.repository;

import com.anoyi.mongo.model.WebSocketSessionBean;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface WebSocketRepository extends MongoRepository<WebSocketSessionBean, String> {

    void deleteByKeyAndValue(String key, String value);

    void deleteByKey(String key);

    WebSocketSessionBean findByKey(String key);

}
