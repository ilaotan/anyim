#### 系统架构图

![](https://upload-images.jianshu.io/upload_images/3424642-7b752a6fcfe81737.png?imageMogr2/auto-orient/)

#### 快速部署

**1、Swarm 集群构建**

**2、MongoDB 集群构建**

**3、Nats 集群构建**

**4、ELK 集群构建**

**5、监控系统构建**

**6、ANOYI-IM 集群构建**

#### 开发注意事项

1、使用 Maven 安装 `spring-boot-starter-nats` 到本地
- github 地址：https://github.com/ChinaSilence/spring-boot-starter-nats
- gitee 地址：https://gitee.com/yyd-code/spring-boot-starter-nats

#### 联系作者
- 微信 VIPshaohan
- 邮箱 545544032@qq.com